﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace task
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private List<User> usersList;
        
        const string connectString = "server = 5.175.134.167; database = TaskManager;" +
                "user = posone; password = IST11;";
        MySqlConnectionStringBuilder mysqlCSB;
        

        private void button1_Click(object sender, EventArgs e)
        {
            User trueEnter = Authorization();
            if (trueEnter != null)
            {
                TaskManager TM = new TaskManager(trueEnter);
                TM.ShowDialog();
            }
        }

        private User Authorization()
        {
            bool trueMail = false;
            foreach (var user in usersList)
            {
                if (user.Email == Mail.Text)
                {
                    trueMail = true;
                    if (user.Pass == Pass.Text)
                    {
                        return new User(user.Id, user.Name,null ,user.Email);
                    }
                    else MessageBox.Show("Неверный пароль");
                }
            }
            if (!trueMail)
            {
                MessageBox.Show("Пользователь не найден");
            }
            return null;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Registration regForm = new Registration(usersList);
            regForm.ShowDialog();
            AddInUserList();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            AddInUserList();
        }

        private void AddInUserList()
        {
            usersList = new List<User>();
            mysqlCSB = new MySqlConnectionStringBuilder();

            MySqlConnection con = new MySqlConnection(connectString);
            con.Open();
            string query = "SELECT * FROM tbl_User";
            MySqlCommand com = new MySqlCommand(query, con);
            MySqlDataReader read = com.ExecuteReader();
            while (read.Read())
            {
                usersList.Add(new User(Convert.ToInt16(read[0].ToString()), read[1].ToString(), read[2].ToString(), read[3].ToString()));
            }
            con.Close();
        }

    }

    
}
