﻿namespace task
{
    partial class NewTask
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.l_nt = new System.Windows.Forms.Label();
            this.l_d = new System.Windows.Forms.Label();
            this.l_dl = new System.Windows.Forms.Label();
            this.l_u = new System.Windows.Forms.Label();
            this.l_p = new System.Windows.Forms.Label();
            this.task_name = new System.Windows.Forms.TextBox();
            this.task_description = new System.Windows.Forms.TextBox();
            this.OK_btn = new System.Windows.Forms.Button();
            this.task_project = new System.Windows.Forms.ComboBox();
            this.erase_btn = new System.Windows.Forms.Button();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.task_deadline = new System.Windows.Forms.TextBox();
            this.task_status = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // l_nt
            // 
            this.l_nt.AutoSize = true;
            this.l_nt.Location = new System.Drawing.Point(24, 57);
            this.l_nt.Name = "l_nt";
            this.l_nt.Size = new System.Drawing.Size(57, 13);
            this.l_nt.TabIndex = 0;
            this.l_nt.Text = "Название";
            // 
            // l_d
            // 
            this.l_d.AutoSize = true;
            this.l_d.Location = new System.Drawing.Point(24, 98);
            this.l_d.Name = "l_d";
            this.l_d.Size = new System.Drawing.Size(57, 13);
            this.l_d.TabIndex = 1;
            this.l_d.Text = "Описание";
            // 
            // l_dl
            // 
            this.l_dl.AutoSize = true;
            this.l_dl.Location = new System.Drawing.Point(25, 139);
            this.l_dl.Name = "l_dl";
            this.l_dl.Size = new System.Drawing.Size(52, 13);
            this.l_dl.TabIndex = 2;
            this.l_dl.Text = "Дедлайн";
            // 
            // l_u
            // 
            this.l_u.AutoSize = true;
            this.l_u.Location = new System.Drawing.Point(24, 21);
            this.l_u.Name = "l_u";
            this.l_u.Size = new System.Drawing.Size(80, 13);
            this.l_u.TabIndex = 3;
            this.l_u.Text = "Пользователь";
            // 
            // l_p
            // 
            this.l_p.AutoSize = true;
            this.l_p.Location = new System.Drawing.Point(24, 183);
            this.l_p.Name = "l_p";
            this.l_p.Size = new System.Drawing.Size(44, 13);
            this.l_p.TabIndex = 4;
            this.l_p.Text = "Проект";
            // 
            // task_name
            // 
            this.task_name.Location = new System.Drawing.Point(117, 54);
            this.task_name.Name = "task_name";
            this.task_name.Size = new System.Drawing.Size(144, 20);
            this.task_name.TabIndex = 5;
            // 
            // task_description
            // 
            this.task_description.Location = new System.Drawing.Point(117, 95);
            this.task_description.Name = "task_description";
            this.task_description.Size = new System.Drawing.Size(144, 20);
            this.task_description.TabIndex = 6;
            // 
            // OK_btn
            // 
            this.OK_btn.Location = new System.Drawing.Point(28, 276);
            this.OK_btn.Name = "OK_btn";
            this.OK_btn.Size = new System.Drawing.Size(70, 25);
            this.OK_btn.TabIndex = 9;
            this.OK_btn.Text = "Готово";
            this.OK_btn.UseVisualStyleBackColor = true;
            this.OK_btn.Click += new System.EventHandler(this.OK_btn_Click);
            // 
            // task_project
            // 
            this.task_project.FormattingEnabled = true;
            this.task_project.Location = new System.Drawing.Point(117, 180);
            this.task_project.Name = "task_project";
            this.task_project.Size = new System.Drawing.Size(144, 21);
            this.task_project.TabIndex = 10;
            // 
            // erase_btn
            // 
            this.erase_btn.Location = new System.Drawing.Point(110, 276);
            this.erase_btn.Name = "erase_btn";
            this.erase_btn.Size = new System.Drawing.Size(70, 25);
            this.erase_btn.TabIndex = 11;
            this.erase_btn.Text = "Сброс";
            this.erase_btn.UseVisualStyleBackColor = true;
            this.erase_btn.Click += new System.EventHandler(this.erase_btn_Click);
            // 
            // cancel_btn
            // 
            this.cancel_btn.Location = new System.Drawing.Point(192, 276);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(70, 25);
            this.cancel_btn.TabIndex = 12;
            this.cancel_btn.Text = "Отмена";
            this.cancel_btn.UseVisualStyleBackColor = true;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // task_deadline
            // 
            this.task_deadline.Location = new System.Drawing.Point(117, 136);
            this.task_deadline.Name = "task_deadline";
            this.task_deadline.Size = new System.Drawing.Size(144, 20);
            this.task_deadline.TabIndex = 15;
            // 
            // task_status
            // 
            this.task_status.AutoSize = true;
            this.task_status.Location = new System.Drawing.Point(28, 231);
            this.task_status.Name = "task_status";
            this.task_status.Size = new System.Drawing.Size(83, 17);
            this.task_status.TabIndex = 16;
            this.task_status.Text = "Выполнена";
            this.task_status.UseVisualStyleBackColor = true;
            // 
            // NewTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(302, 351);
            this.Controls.Add(this.task_status);
            this.Controls.Add(this.task_deadline);
            this.Controls.Add(this.cancel_btn);
            this.Controls.Add(this.erase_btn);
            this.Controls.Add(this.task_project);
            this.Controls.Add(this.OK_btn);
            this.Controls.Add(this.task_description);
            this.Controls.Add(this.task_name);
            this.Controls.Add(this.l_p);
            this.Controls.Add(this.l_u);
            this.Controls.Add(this.l_dl);
            this.Controls.Add(this.l_d);
            this.Controls.Add(this.l_nt);
            this.Name = "NewTask";
            this.Text = "Создание новой задачи";
            this.Load += new System.EventHandler(this.NewTask_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l_nt;
        private System.Windows.Forms.Label l_d;
        private System.Windows.Forms.Label l_dl;
        private System.Windows.Forms.Label l_u;
        private System.Windows.Forms.Label l_p;
        private System.Windows.Forms.TextBox task_name;
        private System.Windows.Forms.TextBox task_description;
        private System.Windows.Forms.Button OK_btn;
        private System.Windows.Forms.ComboBox task_project;
        private System.Windows.Forms.Button erase_btn;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.TextBox task_deadline;
        private System.Windows.Forms.CheckBox task_status;
    }
}