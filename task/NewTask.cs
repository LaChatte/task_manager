﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace task
{
    public partial class NewTask : Form
    {
        const string connectionString = "server = 5.175.134.167; database = TaskManager;" +
                "user = posone; password = IST11;";
        int user_id = -1;
        MySqlConnectionStringBuilder mysqlSB;
        public NewTask(int us_id)
        {
            InitializeComponent();
            user_id = us_id;
        }

        private void OK_btn_Click(object sender, EventArgs e)
        {
            AddNewTask();
            this.Close();
        }

        private void AddNewTask()
        {
            int status = 0;
            if (task_status.Checked) status = 1;
            string temp = task_project.SelectedItem.ToString();
            int k = temp.IndexOf('_');
            int project_id = Convert.ToInt16(temp.Substring(0, k));
            temp = l_u.Text;
            k = temp.IndexOf('_') + 2;
            int u_id = Convert.ToInt16(temp.Substring(k, temp.Length - k));
            mysqlSB = new MySqlConnectionStringBuilder();
            MySqlConnection sql_con = new MySqlConnection(connectionString);
            try
            {
                string query = "INSERT INTO tbl_Task (name, description, deadline, id_user, id_project, status) VALUES ('" +
                                task_name.Text + "', '" + task_description.Text + "', " + Convert.ToInt64(task_deadline.Text) + 
                                ", " + u_id + ", "+ project_id + "," + status +");";
                MySqlCommand command = new MySqlCommand(query, sql_con);
                sql_con.Open();
                command.ExecuteNonQuery();

                MessageBox.Show("Данные добавлены!");
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void NewTask_Load(object sender, EventArgs e)
        {
            l_u.Text = "Пользователь__" + user_id; 
            mysqlSB = new MySqlConnectionStringBuilder();
            MySqlConnection sql_con = new MySqlConnection(connectionString);
            sql_con.Open();
            string query = "SELECT tbl_Project.id, tbl_Project.Name FROM tbl_Project;";
            MySqlCommand command = new MySqlCommand(query, sql_con);
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                task_project.Items.Add(reader[0].ToString() + "__" + reader[1].ToString());
            }
            sql_con.Close();
        }

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void erase_btn_Click(object sender, EventArgs e)
        {
            task_name.Text = String.Empty;
            task_description.Text = String.Empty;
            task_deadline.Text = String.Empty;
            task_deadline.Text = String.Empty;
            task_project.Text = String.Empty;
        }
    }
}
