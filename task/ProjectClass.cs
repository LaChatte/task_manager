﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task
{
    class ProjectUser
    {
        public string Id;
        public string NameProject;
        public string Deadline;
        public string Status;
        public string Description;

        public ProjectUser(string id, string nameProject, string description, string deadline, string status)
        {
            Id = id;
            NameProject = nameProject;
            Deadline = deadline;
            Status = status;
            Description = description;
        }
    }

    class TaskUser
    {
        public string Id;
        public string NameTask;
        public string Deadline;
        public string Status;
        public string Description;

        public TaskUser(string id, string nameTask, string description, string deadline, string status)
        {
            NameTask = nameTask;
            Deadline = deadline;
            Status = status;
            Description = description;
        }
    }

    class CommentsUsers
    {
        public string Text;
        public string TimeCreate;

        public CommentsUsers(string text, string timeCraete)
        {
            Text = text;
            TimeCreate = timeCraete;//(int)(timeCraete - new DateTime(1970, 1, 1)).TotalSeconds;
        }
    }
}
