﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using MySql.Data.MySqlClient;

namespace task
{
    public partial class TaskManager : Form
    {
        private User user;
        public TaskManager(User trueEnter)
        {
            user = trueEnter;
            InitializeComponent();
        }

        private List<ProjectUser> projectsUser;
        private List<TaskUser> tasksUser;
        private List<CommentsUsers> commentsUsers = new List<CommentsUsers>();

        const string connectString = "server = 5.175.134.167; database = TaskManager;" +
                "user = posone; password = IST11;";
        MySqlConnectionStringBuilder mysqlCSB;


        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void TaskManager_Load(object sender, EventArgs e)
        {
            AddProjectInList();
            AddToForm();
        }

        private void AddToForm()
        {
            foreach (var project in projectsUser)
            {
                projectList.Items.Add(project.NameProject);
            }
        }

        private void AddProjectInList()
        {
            projectsUser = new List<ProjectUser>();
            mysqlCSB = new MySqlConnectionStringBuilder();

            MySqlConnection con = new MySqlConnection(connectString);
            con.Open();
            string query = 
                "SELECT id, name, description, deadline, status FROM tbl_Project WHERE (tbl_Project.id_user = " + user.Id + ");";
            MySqlCommand com = new MySqlCommand(query, con);
            MySqlDataReader read = com.ExecuteReader();
            while (read.Read())
            {
                projectsUser.Add(new ProjectUser(read[0].ToString(), read[1].ToString(), read[2].ToString(),
                    read[3].ToString(), read[4].ToString()));
            }
            con.Close();
        }

        private void AddTask()
        {
            tasksUser = new List<TaskUser>();
            foreach (var project in projectsUser)
            {
                if (project.NameProject == projectList.Items[TabIndex])
                {
                    mysqlCSB = new MySqlConnectionStringBuilder();

                    MySqlConnection con = new MySqlConnection(connectString);
                    con.Open();
                    string query = "SELECT tbl_Task.id, tbl_Task.name, tbl_Task.description, tbl_Task.deadline, " +
                        "tbl_Task.status " +
                        "FROM `tbl_Task` " +
                        "WHERE ((tbl_Task.id_user = " + user.Id + ") AND (tbl_Task.id_project = " + project.Id + "));";
                    MySqlCommand com = new MySqlCommand(query, con);
                    MySqlDataReader read = com.ExecuteReader();
                    while (read.Read())
                    {
                        tasksUser.Add(new TaskUser(read[0].ToString(), read[1].ToString(), read[2].ToString(),
                            read[3].ToString(), read[4].ToString()));
                    }
                    con.Close();
                }
            }
        }

        private void projectList_SelectedIndexChanged(object sender, EventArgs e)
        {
            taskBox.Clear();
            AddTask();
            foreach (var task in tasksUser)
            {
                taskList.Items.Add(task.NameTask);
            }
        }

        private void taskList_SelectedIndexChanged(object sender, EventArgs e)
        {
            foreach (var task in tasksUser)
            {
                taskBox.Text = task.NameTask + Environment.NewLine + task.Description;
            }
        }

        private void задачиToolStripMenuItem_Click(object sender, EventArgs e)
        {
           // this.Visible = false;
            NewTask nt = new NewTask(user.Id);
          //  nt.Owner = this;           
            nt.Show();

            /* this.Visible = false;
            Selection sel_f = new Selection();
            sel_f.Owner = this;
            sel_f.Show();
        }*/
        }


    }
}
